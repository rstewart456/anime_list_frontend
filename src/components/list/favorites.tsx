import { useQuery } from "@apollo/client";
import { Card, Container, Spinner, Table } from "react-bootstrap";
import { GET_FAVORITES } from '../querys/query'

function Favorites() {

  const { loading, error, data } = useQuery(GET_FAVORITES);

  if (loading) return <Spinner animation="border" />;

  if (error) return <div>Error</div>;

  const lgStyle = {
    overflowY: 'scroll',
    height: '500px'
  }

  return (
    <>
    {data ? 
      <Container>
        <div className="d-flex d-lg-none flex-wrap">
          {data.favorite.map((favorite: any) => (
            <div className="p-4" key={favorite.id}>
              <Card style={{ width: "8rem" }}>
                <Card.Img variant="top" src={favorite.anime.image} />
                <Card.Body>
                  <Card.Title>{favorite.anime.title}</Card.Title>
                </Card.Body>
              </Card>
            </div>
          ))}
        </div>
        <div className='d-none d-lg-block text-center h3 font-weight-bold'>Favorites</div>
        <div style={lgStyle} className='d-none d-lg-block border border-primary mx-2'>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Image</th>
                <th>Title</th>
                <th>Rating</th>
              </tr>
            </thead>
            <tbody>
            {data.favorite.map((favorite: any) => (
              <tr key={favorite.id}>
                <td><img height={125} src={favorite.anime.image} /></td>
                <td>{favorite.anime.title}</td>
                <td>{favorite.rating}</td>
              </tr>
          ))}
          </tbody>
          </Table>
        </div>
      </Container>
      : null }
    </>
  );
}

export default Favorites;
