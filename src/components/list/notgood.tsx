import { useQuery } from "@apollo/client";
import { Card, Container, Spinner } from "react-bootstrap";
import { GET_NOTGOOD } from '../querys/query'

function Notgood() {

  const { loading, error, data } = useQuery(GET_NOTGOOD);

  if (loading) return <Spinner animation="border" />;

  if (error) return <div>Error</div>;

  const lgStyle = {
    overflowY: 'scroll',
    height: '500px'
  }

  return (
    <>
    {data ?
      <Container>
        <div className="d-flex d-lg-none flex-wrap">
          {data.notgood.map((notgood: any) => (
            <div className="p-4" key={notgood.id}>
              <Card style={{ width: "14rem" }}>
                <Card.Img variant="top" src={notgood.anime.image} />
                <Card.Body>
                  <Card.Title>{notgood.anime.title}</Card.Title>
                </Card.Body>
              </Card>
            </div>
          ))}
        </div>
        <div className='text-center h3 font-weight-bold'>Not Good</div>
        <div style={lgStyle} className='d-none d-lg-block border border-primary mx-2'>
          {data.notgood.map((notgood: any) => (
            <div className="p-4 d-flex align-items-center" key={notgood.id}>
              <img src={notgood.anime.image} alt='anime' height={100} />
              <h6 className='m-4'>{notgood.anime.title}</h6>
            </div>
          ))}
        </div>
      </Container>
      : null }
    </>
  );
}

export default Notgood;