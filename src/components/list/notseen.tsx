import { useQuery } from "@apollo/client";
import { Card, Container, Spinner } from "react-bootstrap";
import { GET_NOTSEEN} from '../querys/query'

function Notseen() {

  const { loading, error, data } = useQuery(GET_NOTSEEN);

  if (loading) return <Spinner animation="border" />;

  if (error) return <div>Error</div>;

  const lgStyle = {
    overflowY: 'scroll',
    height: '500px'
  }

  return (
    <>
    {data ?
      <Container>
        <div className="d-flex d-lg-none flex-wrap">
          {data.notseen.map((notseen: any) => (
            <div className="p-4" key={notseen.id}>
              <Card style={{ width: "14rem" }}>
                <Card.Img variant="top" src={notseen.anime.image} />
                <Card.Body>
                  <Card.Title>{notseen.anime.title}</Card.Title>
                </Card.Body>
              </Card>
            </div>
          ))}
        </div>
        <div className='text-center h3 font-weight-bold'>Not Seen</div>
        <div style={lgStyle} className='d-none d-lg-block border border-primary mx-2'>
          {data.notseen.map((notseen: any) => (
            <div className="p-4 d-flex align-items-center" key={notseen.id}>
              <img src={notseen.anime.image} alt='anime' height={100} />
              <h6 className='m-4'>{notseen.anime.title}</h6>
            </div>
          ))}
        </div>
      </Container>
      : null}
    </>
  );
}

export default Notseen;