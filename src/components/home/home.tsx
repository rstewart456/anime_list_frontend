import { useState } from "react";
import { Button, Container, Card, Modal, Tooltip, OverlayTrigger, Form } from "react-bootstrap";
import { gql, useLazyQuery } from "@apollo/client";
import { ADDANIME } from '../anime/anime'
import FAVORITES from '../list/favorites'
import NOTSEEN from '../list/notseen'
import NOTGOOD from '../list/notgood'

function AnimeInfo(props: any) {

  const { addToAnime } = ADDANIME()

  const favorite = 'favorite'
  const notseen = 'notseen'
  const notgood = 'notgood'

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {props.anime.title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="d-flex">
          <img src={props.anime.image} height={"300"} />
          <div className="px-4">
            <p>{props.anime.description}</p>
            <div>Release in {props.anime.release}</div>
            <div>{props.anime.genres}</div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={() => {
          addToAnime(props.anime, favorite),
          props.onHide()}}>Favorite</Button>
        <Button onClick={() => {
          addToAnime(props.anime, notseen),
          props.onHide()}}>Not Seen</Button>
        <Button onClick={() => {
          addToAnime(props.anime, notgood),
          props.onHide()}}>Not Good</Button>
      </Modal.Footer>
    </Modal>
  );
}

function Home() {
  const [search, setSearch] = useState("");
  const [animePage, setPage] = useState(1);
  const [animePerPage, setPerPage] = useState(10);
  const [animeInfo, setAnime] = useState("");

  const [modalShow, setModalShow] = useState(false);

  const GET_SEARCH = gql`
    query ($search: String!, $page: Int, $perPage: Int) {
      searches(search: $search, page: $page, perPage: $perPage) {
        animes {
          id
          anilistId
          title
          image
          description
          release
          genres
          episodes
        }
      }
    }
  `;

  const [searchAnime, { loading, data, error }] = useLazyQuery(GET_SEARCH);

  const handleSubmit = (e: any) => {
      e.preventDefault();
      searchAnime({
        variables: { search: search, page: animePage, perPage: animePerPage },
      });
      setSearch("");
  };

  return (
    <Container>
      <div className="pt-5">
        <h1 className='text-center'>Search for Anime</h1>
        <Form className='d-flex flex-column justify-content-center align-items-center' onSubmit={handleSubmit}>
          <Form.Control
                    className="w-100 my-3"
                    placeholder="Seach"
                    value={search}
                    onChange={(e) => setSearch(e.target.value)}
                    />
            <Button className="mx-4 w-25" variant="primary" onClick={handleSubmit}>Search</Button>
        </Form>
      </div>
      <div className="pt-5 d-flex flex-wrap justify-content">
        {data
          ? data.searches.animes.map((anime: any) => (
            <div className="p-4" key={anime.id}>
              <OverlayTrigger
                placement="auto"
                delay={{ show: 250, hide: 400 }}
                overlay={
                  <Tooltip id="button-tooltip">
                    {anime.description}
                  </Tooltip>
                }
              >
                <Card style={{ width: "10rem" }}>
                  <Card.Img
                    onClick={() => {
                      setAnime(anime);
                      setModalShow(true);
                    }}
                    variant="top"
                    src={anime.image}
                    style={{height: '200px'}}
                  />
                  <Card.Body className="bg-dark text-white">
                    <Card.Title style={{height: '50px'}}>{anime.title}</Card.Title>
                  </Card.Body>
                </Card>
              </OverlayTrigger>
            </div>
          ))
          : null}
        <AnimeInfo
          show={modalShow}
          anime={animeInfo}
          onHide={() => setModalShow(false)}
        />
      </div>
      <div className='d-none d-lg-flex flex-row justify-content-between my-4'>
        <FAVORITES />
        <NOTSEEN />
        <NOTGOOD />
      </div>
    </Container>
  );
}

export default Home;
