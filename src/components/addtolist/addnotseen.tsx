import { gql, useMutation } from '@apollo/client'
import { GET_FAVORITES, GET_NOTSEEN, GET_NOTGOOD } from '../querys/query'

export const ADD_NOTSEEN = gql`
mutation($anime: String!) {
    addNotseen(anime: $anime) {
        id
    }
}
`
export function addNotseen () {
    const [ addmyNotseen, { data, loading, error}] = useMutation(ADD_NOTSEEN, {
        refetchQueries: [
            {query: GET_FAVORITES},
            {query: GET_NOTSEEN},
            {query: GET_NOTGOOD}
        ],
    })

    const addtoNotseen = (id: String) => {
        try {
            addmyNotseen({
                variables: {
                    anime: id
                }
            })
            //console.log(id)
        } catch (e) {
            console.log(e)
        }
    } 


    return { addtoNotseen, data, error }
}