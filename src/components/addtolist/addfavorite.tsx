import { gql, useMutation } from '@apollo/client'
import { GET_FAVORITES, GET_NOTSEEN, GET_NOTGOOD } from '../querys/query'

export const ADD_FAVORITE = gql`
mutation($anime: String!) {
    addFavorite(anime: $anime) {
        id
    }
}
`
export function addFavorite () {
    const [ addmyFavorite, { data, loading, error}] = useMutation(ADD_FAVORITE, {
        refetchQueries: [
            {query: GET_FAVORITES},
            {query: GET_NOTSEEN},
            {query: GET_NOTGOOD}
        ],
        //update(cache, { data }) {
        //    const { notseen } = cache.readQuery({
        //        query: GET_NOTSEEN
        //    })
        //    console.log(data)
        //    cache.writeQuery({
        //        query: GET_NOTSEEN,
        //        data: {
        //            notseen: notseen.filter(not => 
        //                not.anime.id !== data.addmyFavorite.anime)
        //        }
        //    })
        //}
    })

    const addtoFavorite = (id: String) => {
        try {
            addmyFavorite({
                variables: {
                    anime: id
                }
            })
        } catch (e) {
            console.log(e)
        }
    } 


    return { addtoFavorite, data, error }
}