import { gql, useMutation } from '@apollo/client'
import { GET_FAVORITES, GET_NOTSEEN, GET_NOTGOOD } from '../querys/query'

export const ADD_NOTGOOD = gql`
mutation($anime: String!) {
    addNotgood(anime: $anime) {
        id
    }
}
`
export function addNotgood () {
    const [ addmyNotgood, { data, loading, error}] = useMutation(ADD_NOTGOOD, {
        refetchQueries: [
            {query: GET_FAVORITES},
            {query: GET_NOTSEEN},
            {query: GET_NOTGOOD}
        ]
    })

    const addtoNotgood = (id: String) => {
        try {
            addmyNotgood({
                variables: {
                    anime: id
                }
            })
            //console.log(id)
        } catch (e) {
            console.log(e)
        }
    } 


    return { addtoNotgood, data, error }
}