import { gql, useMutation } from '@apollo/client'
import { Button } from 'react-bootstrap'
import { addFavorite } from '../addtolist/addfavorite'
import { addNotseen } from '../addtolist/addnotseen'
import { addNotgood } from '../addtolist/addnotgood'
import { ADD_ANIME } from '../querys/query'

interface ANIME {
    ani: {
        anilistId: Number,
        title: String,
        image: String,
        description: String,
        genres: String[],
        release: Number
        episodes: Number
    },
}

export function ADDANIME() {
    const [addMyAnime, { data, loading, error }] = useMutation(ADD_ANIME);

    const { addtoFavorite } = addFavorite();
  
    const { addtoNotseen } = addNotseen();
  
    const { addtoNotgood } = addNotgood();
  
    const addToAnime = (ani: ANIME, list: String) => {
        addMyAnime({
          variables: {
            anilistId: ani.anilistId,
            title: ani.title,
            image: ani.image,
            description: ani.description,
            genres: ani.genres,
            release: ani.release,
            episodes: ani.episodes
          },
        }).then(List => {
          if(list === 'favorite') {
          addtoFavorite(List.data.addAnime.id)
          }
          if(list === 'notseen') {
            addtoNotseen(List.data.addAnime.id)
          }
          if(list === 'notgood') {
            addtoNotgood(List.data.addAnime.id)
          }
        }).catch((e) => console.log(e))
    };
  
    return { addToAnime, data, error };
}