import { useLazyQuery } from '@apollo/client'
import { GET_NOTGOOD } from '../querys/query'

export function getNotgood () {
    const [ getmyNotgood, { loading, data, error}] = useLazyQuery(GET_NOTGOOD)

    return { getmyNotgood, loading, gDB: data, error }
}