import { useLazyQuery } from '@apollo/client'
import { GET_NOTSEEN } from '../querys/query'

export function getNotseen () {
    const [ getmyNotseen, { loading, data, error}] = useLazyQuery(GET_NOTSEEN)

    return { getmyNotseen, loading, data, error }
}