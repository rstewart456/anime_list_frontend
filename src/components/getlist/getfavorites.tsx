import { useLazyQuery } from '@apollo/client'
import { GET_FAVORITES } from '../querys/query'

export function getFavorites () {
    const [ getmyFavorites, { loading, data, error}] = useLazyQuery(GET_FAVORITES)

    return { getmyFavorites, loading, data, error }
}