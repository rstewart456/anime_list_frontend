import { gql } from '@apollo/client'

export const GET_FAVORITES = gql`
query {
  favorite {
    id
    rating
    anime {
      title
      image
    }
  }
}
`;

export const GET_NOTSEEN = gql`
query {
  notseen {
    id
    rating
    anime {
      title
      image
    }
  }
}
`;

export const GET_NOTGOOD = gql`
query {
  notgood {
    id
    rating
    anime {
      title
      image
    }
  }
}
`;

export const ADD_ANIME = gql`
mutation (
  $anilistId: Int!
  $title: String!
  $image: String
  $description: String
  $genres: [String]
  $release: Int
  $episodes: Int
) {
  addAnime(
    anilistId: $anilistId
    title: $title
    image: $image
    description: $description
    genres: $genres
    release: $release
    episodes: $episodes
  ) {
    id
  }
}
`;

export const GET_USER = gql`
query {
  user {
    username
  }
}
`;