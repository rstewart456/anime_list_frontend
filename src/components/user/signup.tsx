import { useState } from "react";
import * as yup from "yup";
import { Formik } from "formik";
import { Nav, Modal, Form, Button } from "react-bootstrap";
import { gql, useMutation } from "@apollo/client";

function Signup() {
  const [show, setShow] = useState(false);

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const basicSchema = yup.object().shape({
    email: yup.string().email("Please enter email").required("Invalid Email"),
    username: yup
      .string()
      .min(2, "Must have 2 Characters")
      .required("username is required"),
    password: yup
      .string()
      .min(6, "Password must be at last 6 characters")
      .required("Passowrd is requried"),
    confirmPassword: yup
      .string()
      .oneOf([yup.ref("password"), null], "Password must match")
      .required("Confirm password is required"),
  });

  const SIGN_UP = gql`
    mutation (
      $email: String!
      $username: String!
      $password: String!
      $confirmPassword: String!
    ) {
      addUser(
        email: $email
        username: $username
        password: $password
        confirmPassword: $confirmPassword
      ) {
        id
        username
        token
      }
    }
  `;

  const [signUp, { data, error }] = useMutation(SIGN_UP);

  const Submit = (event: any) => {
    signUp({
      variables: {
        email: event.email,
        username: event.username,
        password: event.password,
        confirmPassword: event.confirmPassword,
      },
    });
    handleClose();
  };

  if (error) {
    console.log(error);
  }

  if (data) {
    localStorage.setItem("token", data.addUser.token);
  }

  return (
    <>
      <Nav.Link onClick={handleShow}>Sign Up</Nav.Link>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Sign Up</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Formik
            validationSchema={basicSchema}
            onSubmit={Submit}
            initialValues={{
              email: "",
              username: "",
              password: "",
              confirmPassword: "",
            }}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              values,
              touched,
              isValid,
              errors,
            }) => (
              <Form onSubmit={handleSubmit}>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    name="email"
                    placeholder="name@example.com"
                    value={values.email}
                    onChange={handleChange}
                    isInvalid={!!errors.email}
                    autoFocus
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.email}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Label>User Name</Form.Label>
                  <Form.Control
                    type="text"
                    name="username"
                    value={values.username}
                    onChange={handleChange}
                    placeholder="name@example.com"
                    isInvalid={!!errors.username}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.username}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    name="password"
                    value={values.password}
                    onChange={handleChange}
                    placeholder="Password"
                    isInvalid={!!errors.password}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.password}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Label>Confirmed Password</Form.Label>
                  <Form.Control
                    type="password"
                    name="confirmPassword"
                    value={values.confirmPassword}
                    onChange={handleChange}
                    placeholder="Confirm Password"
                    isInvalid={!!errors.confirmPassword}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.confirmPassword}
                  </Form.Control.Feedback>
                </Form.Group>
                <Button className="mx-3" variant="primary" type='submit'>
                  Create User
                </Button>
              </Form>
            )}
          </Formik>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default Signup;
