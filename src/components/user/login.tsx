import { useState } from "react";
import { GET_FAVORITES, GET_NOTSEEN, GET_NOTGOOD, GET_USER } from '../querys/query'
import { Nav, Modal, Form, Button } from "react-bootstrap";
import { gql, useMutation } from "@apollo/client";
import { Formik } from "formik";
import * as yup from "yup";

function Login() {

  interface LOGIN {
    email: String,
    password: String
  }

  const [show, setShow] = useState(false);

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const basicSchema = yup.object().shape({
    email: yup
      .string()
      .email("Please enter a valid email")
      .required("Required"),
    password: yup.string().required("Required"),
  });

  const LOG_IN = gql`
    mutation LogIn($email: String!, $password: String!) {
      logIn(email: $email, password: $password) {
        id
        username
        token
      }
    }
  `;

  const [logIn, { data, error }] = useMutation(LOG_IN, {
    refetchQueries: [
      {query: GET_USER},
      {query: GET_FAVORITES},
      {query: GET_NOTSEEN},
      {query: GET_NOTGOOD}
    ]
  });

  const Submit = (event: LOGIN) => {
    logIn({ variables: { email: event.email, password: event.password } });
    handleClose();
  };

  if (error) {
    console.log(error);
  }

  if (data) {
    localStorage.setItem("token", data.logIn.token);
  }

  return (
    <>
      <Nav.Link onClick={handleShow}>Log In</Nav.Link>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Log In</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Formik
            validationSchema={basicSchema}
            onSubmit={Submit}
            initialValues={{
              email: "",
              password: "",
            }}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              values,
              touched,
              isValid,
              errors,
            }) => (
              <Form onSubmit={handleSubmit}>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="name@example.com"
                    autoFocus
                    name="email"
                    value={values.email}
                    onChange={handleChange}
                    isInvalid={!!errors.email}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.email}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    name="password"
                    placeholder="Password"
                    value={values.password}
                    onChange={handleChange}
                    isInvalid={!!errors.password}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.password}
                  </Form.Control.Feedback>
                </Form.Group>
                <Button className="my-2" variant="primary" type="submit">
                  Submit
                </Button>
              </Form>
            )}
          </Formik>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default Login;
