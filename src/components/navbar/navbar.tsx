import { useState } from 'react'
import { Nav, Navbar, Container, Offcanvas, Button } from "react-bootstrap";
import { useQuery } from "@apollo/client";
import { Link } from "react-router-dom";
import { GET_USER } from '../querys/query'
import Login from "../user/login";
import Signup from "../user/signup";

function NavBar() {


  const [menuOpen, setMenuOpen] = useState(false)
  const toggleMenu = () => {
    setMenuOpen(!menuOpen)
  }

  const handleClose= () => setMenuOpen(false)

  const {client, loading, error, data } = useQuery(GET_USER);

  function logout() {
      localStorage.removeItem('token')
      client.resetStore()
    }

  return (
    <>
    <Navbar collapseOnSelect bg="dark" variant='dark' fixed='top' expand='lg' className="mb-3">
      <Container fluid>
        <Navbar.Brand href="#">Anime List</Navbar.Brand>
        <Navbar.Toggle 
        aria-controls={`offcanvasNavbar-expand-lg`}
        onClick={toggleMenu}
        />
        <Navbar.Offcanvas
          id={`offcanvasNavbar-expand-lg`}
          aria-labelledby={`offcanvasNavbarLabel-expand-lg`}
          placement="end"
          show={menuOpen ? 1 : 0}
          onHide={handleClose}
        >
          <Offcanvas.Header closeButton onClick={handleClose}>
            <Offcanvas.Title id={`offcanvasNavbarLabel-expand-lg`}>
              Anime List
            </Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body>
            <Nav className="justify-content-start flex-grow-1 pe-3">
              <Nav.Link as={Link} to='/' onClick={handleClose}>Home</Nav.Link>
              <Nav.Link as={Link} to='/favorites' onClick={handleClose}>Favorites</Nav.Link>
              <Nav.Link as={Link} to='/notseen' onClick={handleClose}>Not Seen</Nav.Link>
              <Nav.Link as={Link} to='/notgood' onClick={handleClose}>Not Good</Nav.Link>
            </Nav>
            <Nav>
              <Login /> 
              <Button onClick={logout}>Log Out</Button>
            </Nav>
          </Offcanvas.Body>
        </Navbar.Offcanvas>
      </Container>
    </Navbar>
    </>
  );
}

export default NavBar;
