import NavBar from "./components/navbar/navbar";
import { Routes, Route } from "react-router-dom";
import {
  ApolloProvider,
  ApolloClient,
  InMemoryCache,
  createHttpLink,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
// Components
import Home from "./components/home/home";
import Favorites from "./components/list/favorites";
import Notseen from "./components/list/notseen";
import Notgood from "./components/list/notgood";

function App() {
  const httpLink = createHttpLink({
    uri: "http://localhost:3443",
  });

  const afterwareLink = setContext((_, { headers }) => {
    const token = localStorage.getItem("token");
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : "",
      },
    };
  });

  const link = afterwareLink.concat(httpLink);

  const client = new ApolloClient({
    link,
    cache: new InMemoryCache(),
  });

  return (
    <ApolloProvider client={client}>
      <div className="App">
        <NavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/favorites" element={<Favorites />} />
          <Route path="/notseen" element={<Notseen />} />
          <Route path="/notgood" element={<Notgood />} />
        </Routes>
      </div>
    </ApolloProvider>
  );
}

export default App;
